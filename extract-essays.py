import argparse

import html2text
from bs4 import BeautifulSoup, NavigableString, Tag


def convert_html_to_markdown(html_content):
    converter = html2text.HTML2Text()
    converter.ignore_links = True  # Set to True if you want to ignore converting links
    markdown_content = converter.handle(html_content)
    return markdown_content


def extract_content_between_h2(html_document):
    soup = BeautifulSoup(html_document, 'html.parser')
    content_blocks = []

    h2_tags = soup.find_all('h2')

    for i in range(len(h2_tags)):
        current_h2 = h2_tags[i]
        content = str(current_h2)  # Include the current H2 tag in the content

        # Iterate through sibling elements until the next H2 tag is found
        for sibling in current_h2.next_siblings:
            if sibling and isinstance(sibling, Tag) and sibling.name == 'h2':
                break
            if sibling and (isinstance(sibling, Tag) or
                            isinstance(sibling, NavigableString)):
                content += str(sibling)

        content_blocks.append(content)

    return content_blocks


def cleanup_tags(html_content):
    soup = BeautifulSoup(html_content, 'html.parser')

    # Convert <pre xml:space="preserve"> to <aside>
    for pre_tag in soup.find_all('pre', {'xml:space': 'preserve'}):
        pre_tag.name = 'blockquote'
        del pre_tag['xml:space']  # Remove the xml:space attribute

    # Remove all tags except <h2> and <p>
    for tag in soup.find_all():
        if tag.name not in ['h2', 'p', 'blockquote']:
            if tag.name is not None:
                tag.unwrap()
            else:
                tag.extract()

    return str(soup)


def extract_content(html_file):
    with open(html_file, "r", encoding="utf-8") as file:
        html_content = file.read()
        extracted_contents = extract_content_between_h2(html_content)

        # Process the extracted content as needed, e.g., save to files
        for index, content in enumerate(extracted_contents):
            clean_content = cleanup_tags(content)
            markdown = convert_html_to_markdown(clean_content)
            # with open(f"extracted_content_{index}.html", "w", encoding="utf-8") as out_file:
            #     out_file.write(clean_content)
            with open(f"extracted_content_{index}.md", "w", encoding="utf-8") as out_file:
                out_file.write(markdown)


def main():
    parser = argparse.ArgumentParser(description="HTML Content Extractor")
    parser.add_argument("html_file", help="Path to the HTML file")
    args = parser.parse_args()

    extract_content(args.html_file)


if __name__ == "__main__":
    main()
