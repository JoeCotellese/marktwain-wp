import argparse
import os
from xml.dom import minidom
from xml.etree.ElementTree import Element, SubElement, tostring

import markdown2
import titlecase


def markdown_to_html(markdown_text):
    return markdown2.markdown(markdown_text)


def create_wxr_post(title, content):
    item = Element('item')
    title_elem = SubElement(item, 'title')
    title_elem.text = titlecase.titlecase(title)
    
    wp_namespace = "http://wordpress.org/export/1.2/"
    status_elem = SubElement(item, f'{{{wp_namespace}}}status')
    status_elem.text = "publish"

    post_type_elem = SubElement(item, f'{{{wp_namespace}}}post_type')
    post_type_elem.text = "post"

    # Add namespace declaration
    content_elem = SubElement(item, '{http://purl.org/rss/1.0/modules/content/}encoded')
    content_elem.text = content

    return item


def process_markdown_files(directory):
    rss = Element('rss', {
        'version': '2.0',
        'xmlns:excerpt': 'http://wordpress.org/export/1.2/excerpt/',
        'xmlns:content': 'http://purl.org/rss/1.0/modules/content/',
        'xmlns:wfw': 'http://wellformedweb.org/CommentAPI/',
        'xmlns:wp': 'http://wordpress.org/export/1.2/',
    })
    channel = SubElement(rss, 'channel')
    # Add the WXR version number
    wxr_version_elem = SubElement(channel, 'wp:wxr_version')
    wxr_version_elem.text = "1.2"  # Replace with your desired WXR version

    # Declare the namespace for WordPress specific tags
    channel.set('xmlns:wp', "http://wordpress.org/export/1.2/")
    for filename in os.listdir(directory):
        if filename.endswith(".md"):
            with open(os.path.join(directory, filename), 'r') as file:
                lines = file.readlines()
                title_line = next((line for line in lines if line.startswith("##")), None)
                title = title_line.strip("# ").strip() if title_line else "No Title"
                content = markdown_to_html(''.join(lines))
                channel.append(create_wxr_post(title, content))

    return rss


def prettify_xml(element):
    rough_string = tostring(element, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


def main():
    parser = argparse.ArgumentParser(description='Convert Markdown content to WXR format for WordPress.')
    parser.add_argument('directory', help='Directory containing Markdown files')
    parser.add_argument('output_file', help='Output WXR file name')

    args = parser.parse_args()

    channel = process_markdown_files(args.directory)
    wxr_content = prettify_xml(channel)

    with open(args.output_file, "w") as file:
        file.write(wxr_content)


if __name__ == "__main__":
    main()
